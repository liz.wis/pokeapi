package com.example.pokemon;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.pokemon.model.Pokemon;
import com.example.pokemon.model.SendPokemon;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "prof chen";
    private Retrofit retrofit;
    private RecyclerView recyclerView;
    private Recycler recycler;
    private int offset;
    private boolean app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recycler = new Recycler(this);
        recyclerView.setAdapter(recycler);
        recyclerView.setHasFixedSize(true);
        final GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int visible = layoutManager.getChildCount();
                    int total = layoutManager.getItemCount();
                    int past = layoutManager.findFirstVisibleItemPosition();

                    if (app) {
                        if ((visible + past) >= total) {
                            Log.i(TAG, "blabla");
                            app = false;
                            offset += 20;
                            getData(offset);
                        }
                    }
                }
            }

        });

        retrofit = new Retrofit.Builder()
                .baseUrl("http://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        app = true;
        offset = 0;
        getData(offset);

    }


    private void getData(int offset) {
        Api service = retrofit.create(Api.class);
        Call<SendPokemon>sendPokemonCall = service.pokemonList(20, offset);

        sendPokemonCall.enqueue(new Callback<SendPokemon>() {
            @Override
            public void onResponse(Call<SendPokemon> call, Response<SendPokemon> response) {
                app = true;
                if(response.isSuccessful()) {

                    SendPokemon sendPokemon = response.body();
                    ArrayList<Pokemon>listPoke = sendPokemon.getPokemon();

                    recycler.addList(listPoke);
                } else {
                    Log.e(TAG, "onResponse: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<SendPokemon> call, Throwable t) {
                app = true;
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });

    }
}