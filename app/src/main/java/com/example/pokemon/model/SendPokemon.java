package com.example.pokemon.model;

import java.util.ArrayList;

public class SendPokemon {
    private ArrayList<Pokemon> results;

    public ArrayList<Pokemon> getPokemon() {
        return results;
    }

    public void setPokemon(ArrayList<Pokemon> results) {
        this.results = results;
    }
}
