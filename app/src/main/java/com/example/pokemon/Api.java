package com.example.pokemon;

import com.example.pokemon.model.SendPokemon;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    @GET("pokemon")
    Call<SendPokemon>pokemonList(@Query("limit") int limit, @Query("offset") int offset);
}
